import React, { createContext, useContext, useState, useEffect } from "react";
import { getTickerList } from '../services/Tickers';

const TickerContext = createContext();

export const TickerProvider = ({ children, value }) => {
  const [loading, setLoading] = useState(true);
  const [tickers, setTickers] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      const batches = await getTickerList();
      if (!tickers.error) {
        setTickers(
          batches//.reduce((items, bch) => [...items, bch], [])
        );
      }
      setLoading(false);
    };

    fetch();
  }, []);

  return (
    <TickerContext.Provider
      value={{
        loading,
        tickers,
      }}
    >
      {children}
    </TickerContext.Provider>
  );
};

export const useTicker = () => useContext(TickerContext);

export default TickerContext;
