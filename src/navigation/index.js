import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import TickerStack from './TickerStack';

const Stack = createStackNavigator();

const NavigationApp = (props) => (
  <NavigationContainer>
    <Stack.Navigator {...props}>
      <Stack.Screen name='Home' component={HomeScreen} />
      <Stack.Screen name='Tickers' component={TickerStack} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default NavigationApp;
