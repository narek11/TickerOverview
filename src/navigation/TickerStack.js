import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TickerListScreen from '../screens/TickerListScreen';
import TickerDetailsScreen from '../screens/TickerDetailsScreen';
import { TickerProvider } from '../contexts/TickerContext';

const Stack = createStackNavigator();

const TickerTSack = (props) => (
  <TickerProvider>
    <Stack.Navigator screenOptions={{ headerShown: false }} {...props}>
      <Stack.Screen name='TickerList' component={TickerListScreen} />
      <Stack.Screen name='TickerDetails' component={TickerDetailsScreen} />
    </Stack.Navigator>
  </TickerProvider>
);

export default TickerTSack;
