import React from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Separator from '../../Layout/Separator';
import { colors } from '../../../constants/colors';

const TickerList = ({ tickers, onItemPress }) => (
  <FlatList
    data={tickers}
    keyExtractor={(ticker) => ticker[0]}
    renderItem={({ item: ticker }) => {

      const name = ticker[0];
      return (
        <Ticker name={name} onPress={() => onItemPress(name)} />
      );
    }}
    contentContainerStyle={styles.container}
    ItemSeparatorComponent={() => (<Separator />)}
  />
);

const Ticker = ({ name, onPress }) => (
  <TouchableOpacity style={styles.rowButton} onPress={onPress}>
    <Text style={styles.name}>{name}</Text>
    <Ionicons name='chevron-forward' size={32} color={colors.purple} />
  </TouchableOpacity>
);

const styles = {
  container: {
    marginHorizontal: 10
  },
  rowButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.buttonBackground
  },
  name: {
    padding: 5,
    color: colors.buttonLabel,
    fontSize: 18,
    fontWeight: 'bold'
  }
};
export default TickerList;
