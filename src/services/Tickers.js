import { URLS } from '../constants/api';

export const getTickerList = () => {
  return fetch(`${URLS.TICKER_LIST}?symbols=ALL`)
    .then(responseHandler)
    .catch(errorHandler);
};

export const getTickerDetails = (name) => {
  return fetch(`${URLS.TICKER_DETAILS}/${name}`)
    .then(responseHandler)
    .catch(errorHandler);
};

export const responseHandler = (res) => {
  try {
    return res.json();
  } catch (e) {
    return {
      error: e,
      message: "Invalid JSON Response",
    };
  }
};

export const errorHandler = (e) => {
  console.log(e);
  return {
    error: e,
    message: "Request Failed!",
  };
};
