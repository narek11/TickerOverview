import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { useTicker } from '../contexts/TickerContext';
import TickerList from '../components/Tickers/TickerList';

const TickerListScreen = ({ navigation }) => {
  const { loading, tickers } = useTicker();

  if (loading) {
    return <ActivityIndicator />;
  }

  const onItemPress = (name) => {
    navigation.navigate('TickerDetails', { name });
  };

  return (
    <View style={styles.container}>
      <TickerList tickers={tickers} onItemPress={onItemPress} />
    </View>
  );

};
const styles = {
  container: {
    flex: 1,
    marginTop: 30,
    marginHorizontal: 20
  }
};

export default TickerListScreen;
