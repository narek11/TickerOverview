import React, { useState, useEffect } from 'react';
import { Text, ScrollView, ActivityIndicator, View } from 'react-native';
import { getTickerDetails } from '../services/Tickers';
import { colors } from "../constants/colors";

const TickerDetailsScreen = ({ navigation, route }) => {
  const [loading, setLoading] = useState(true);
  const [values, setValues] = useState([]);
  const { name } = route.params;

  useEffect(() => {
    const fetch = async () => {
      const values = await getTickerDetails(name);
      if (!values.error) {
        setValues(values);
      }
      setLoading(false);
    };

    fetch();
  }, []);

  if (loading) {
    return <ActivityIndicator />;
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.title}>{name}</Text>
        {values.map((vl, index) => (
          <Ticker key={index} value={vl} />
        ))}
      </ScrollView>

    </View>
  );
};

const Ticker = ({ value }) => (
  <Text style={styles.value}>{value}</Text>
);

const styles = {
  container: {
    flex: 1,
    marginTop: 30,
    marginHorizontal: 20,
  },
  title: {
    fontSize: 20
  },
  value: {
    fontSize: 17,
    color: colors.purple

  }
};
export default TickerDetailsScreen;
