import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { colors } from "../constants/colors";

const HomeScreen = ({ navigation }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={() => navigation.navigate('Tickers')} style={styles.button}>
      <Text style={styles.buttonLabel}>Go to Ticker list</Text>
    </TouchableOpacity>
  </View>
);

const styles = {
  container: {
    flex: 1,
    marginTop: 30,
    marginHorizontal: 20
  },
  button: {
    padding: 10,
    backgroundColor: colors.buttonBackground
  },
  buttonLabel: {
    color: colors.buttonLabel,
    fontSize: 18,
    fontWeight: 'bold'
  }

};

export default HomeScreen;
