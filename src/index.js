import React from 'react';

import NavigationApp from './navigation/index';

const App = () => (
  <NavigationApp />
);

export default App;
