const pallete = {
  purple: 'purple',
  green: 'green',
  white: 'white'
};

export const colors = {
  ...pallete,
  buttonBackground: pallete.green,
  buttonLabel: pallete.white
};
