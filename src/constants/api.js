const BASE_URL = 'https://api-pub.bitfinex.com/v2';

export const URLS = {
  TICKER_LIST: `${BASE_URL}/tickers`,
  TICKER_DETAILS: `${BASE_URL}/ticker`
};
